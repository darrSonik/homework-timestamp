/*
    Name : Asaduzzaman Noor
    ID   : 1631231042
*/
#ifndef TIMESTAMP_H_INCLUDED
#define TIMESTAMP_H_INCLUDED
//#include <iostream>
class timeStamp {
    private:
        int seconds;
        int minutes;
        int hours;

    public:
        timeStamp();
        timeStamp(int, int, int);
        void print_time();
        bool operator != (timeStamp);
        bool operator < (timeStamp);
        bool operator > (timeStamp);
        //void* operator new(std::size_t);
        //void operator delete (void*);
};

#endif // TIMESTAMP_H_INCLUDED
