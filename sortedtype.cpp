/*
    Name : Asaduzzaman Noor
    ID   : 1631231042
*/
#include "sortedtype.h"

template <typename itemType>
sortedType<itemType>::sortedType() {
    this->length = 0;
    this->current_position = -1;
}

template <typename itemType>
void sortedType<itemType>::make_empty() {
    this->length = 0;
}

template <typename itemType>
bool sortedType<itemType>::is_full() {
    return (length == MAX_ITEMS);
}

template <typename itemType>
int sortedType<itemType>::get_length() {
    return this->length;
}

template <typename itemType>
void sortedType<itemType>::reset_list() {
    this->current_position = -1;
}

template <typename itemType>
void sortedType<itemType>::get_next_item(itemType& item) {
    this->current_position += 1;
    item = this->info[this->current_position];
}

template <typename itemType>
void sortedType<itemType>::insert_item(itemType item) {
    int location = 0;
    bool more_to_search = (location < this->length);
    while(more_to_search) {
        if(item > this->info[location]) {
            location += 1;
            more_to_search = (location < this->length);
        } else if(item < this->info[location]) {
            more_to_search = false;
        }
    }
    for(int index = this->length; index > location; index -= 1) this->info[index] = this->info[index-1];
    this->info[location] = item;
    length += 1;
}

template <typename itemType>
void sortedType<itemType>::delete_item(itemType item) {
    int location = 0;
    while(item != this->info[location]) location += 1;
    for(int index = location+1; index < this->length; index += 1) this->info[index-1] = this->info[index];
    this->length -= 1;
}

template <typename itemType>
void sortedType<itemType>::retrieve_item(itemType& item, bool& found) {
    int midpoint, first = 0, last = length-1;
    bool more_to_search = (first <= last);
    found = false;
    while(more_to_search && !found) {
        midpoint = (first + last) / 2;
        if(item < this->info[midpoint]) {
            last = midpoint - 1;
            more_to_search = (first <= last);
        } else if(item > this->info[midpoint]) {
            first = midpoint + 1;
            more_to_search = (first <= last);
        } else {
            found = true;
            item = this->info[midpoint];
        }
    }
}
