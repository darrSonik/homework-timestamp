/*
    Name : Asaduzzaman Noor
    ID   : 1631231042
*/
#ifndef SORTEDTYPE_H_INCLUDED
#define SORTEDTYPE_H_INCLUDED

const int MAX_ITEMS = 5;

template <typename itemType>
class sortedType {
    public:
        sortedType();
        void make_empty();
        bool is_full();
        int get_length();
        void insert_item(itemType);
        void delete_item(itemType);
        void retrieve_item(itemType&, bool&);
        void reset_list();
        void get_next_item(itemType&);

    private:
        int length;
        itemType info[MAX_ITEMS];
        int current_position;
};

#endif // SORTEDTYPE_H_INCLUDED
