#include "timestamp.h"
#include <cstdio>

timeStamp::timeStamp() {
    this->seconds = 0;
    this->minutes = 0;
    this->hours = 0;
}

timeStamp::timeStamp(int s, int m, int h) {
    this->seconds = s;
    this->minutes = m;
    this->hours = h;
}

void timeStamp::print_time() {
    printf("%02d:%02d:%02d\n", this->seconds, this->minutes, this->hours);
}

bool timeStamp::operator != (timeStamp t) {
    if(t.hours != this->hours || t.minutes != this->minutes || t.seconds != this->seconds) return true;
    else return false;
}

bool timeStamp::operator > (timeStamp t) {
    if(t.hours < this->hours) return true;
    else if(t.hours == this->hours && t.minutes < this->minutes) return true;
    else if(t.hours == this->hours && t.minutes == this->minutes && t.seconds < this->seconds) return true;
    else return false;
}

bool timeStamp::operator < (timeStamp t) {
    if(t.hours > this->hours) return true;
    else if(t.hours == this->hours && t.minutes > this->minutes) return true;
    else if(t.hours == this->hours && t.minutes == this->minutes && t.seconds > this->seconds) return true;
    else return false;
}

/*void* timeStamp::operator new (size_t size) {
    void* p = malloc(size);
}

void timeStamp::operator delete (void* p) {
    free(p);
}*/