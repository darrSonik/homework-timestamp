/*
    Name : Asaduzzaman Noor
    ID   : 1631231042
*/
#include "sortedtype.cpp"
#include "timestamp.h"
#include <iostream>

int main()
{
    // create a list of integers
    sortedType<int> s_list;

    // print the length of the list
    std::cout << "Length of the list: " << s_list.get_length() << std::endl;

    // insert five items
    s_list.insert_item(5);
    s_list.insert_item(7);
    s_list.insert_item(4);
    s_list.insert_item(2);
    s_list.insert_item(1);

    // print the list
    int el;
    s_list.reset_list();
    for(int i=0; i<s_list.get_length(); i+=1) {
        s_list.get_next_item(el);
        std::cout << el << " ";
    }
    std::cout << std::endl;

    // retrieve 6 and print whether found or not
    bool is_found;
    int to_search = 6;
    s_list.retrieve_item(to_search, is_found);
    is_found ? std::cout << "Item is found" << std::endl : std::cout << "Item is not found" << std::endl;

    // retrieve 5 and print whether found or not
    to_search = 5;
    s_list.retrieve_item(to_search, is_found);
    is_found ? std::cout << "Item is found" << std::endl : std::cout << "Item is not found" << std::endl;

    // print the list is full or not
    s_list.is_full() ? std::cout << "List is full" << std::endl : std::cout << "List is not full" << std::endl;

    // delete 1
    s_list.delete_item(1);

    // print the list
    s_list.reset_list();
    for(int i=0; i<s_list.get_length(); i+=1) {
        s_list.get_next_item(el);
        std::cout << el << " ";
    }
    std::cout << std::endl;

    // print if the list is full or not
    s_list.is_full() ? std::cout << "List is full" << std::endl : std::cout << "List is not full" << std::endl;

// ==============================================================

    timeStamp t1(15, 34, 23);
    timeStamp t2(13, 13, 2);
    timeStamp t3(43, 45, 12);
    timeStamp t4(25, 36, 17);
    timeStamp t5(52, 2, 20);

    sortedType<timeStamp> s_t;
    s_t.insert_item(t1);
    s_t.insert_item(t2);
    s_t.insert_item(t3);
    s_t.insert_item(t4);
    s_t.insert_item(t5);

    std::cout << "\n-------------------------" << std::endl;
    timeStamp t;

    // print the list
    s_t.reset_list();
    for(int i=0; i<s_t.get_length(); i+=1) {
        s_t.get_next_item(t);
        t.print_time();
    }
    
    
    timeStamp temp(25, 36, 17);
    s_t.delete_item(temp);

    //delete &temp;
    
    std::cout << "\n-------------------------" << std::endl;
    // print the list
    s_t.reset_list();
    for(int i=0; i<s_t.get_length(); i+=1) {
        s_t.get_next_item(t);
        t.print_time();
    }
    
    return 0;
}
